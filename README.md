# force-clusterversion

Helm chart to set the desired Openshift ClusterVersion and controls OKD4 upgrades. Used as an ArgoCD Application to update the cluster's default ClusterVersion resource. See https://github.com/openshift/cluster-version-operator

## Why is this a separate Helm chart?

Because the `ClusterVersion/version` manifest is part of an ArgoCD application, ArgoCD also tracks the 800+ resources that are deployed by the CVO.
This in turn significantly slows down the ArgoCD Web UI.
Therefore we keep this manifest (which could also be part of the okd4-install umbrella chart) in a separate satellite chart.
This way, the chart is deployed as a standalone application and the slowness of the Sync / UI is much less of a concern.

Reference: <https://gitlab.cern.ch/paas-tools/okd4-install/-/merge_requests/1270>.
